const faker = require('faker')
const { ObjectId } = require('mongoose').Types

module.exports = [
  {
    _id: new ObjectId('5aa1c2c35ef7a4e97b5e995a'),
    name: 'Administrator',
    email: 'admin@example.com',
    password: '$2a$05$UsjaSYHjAaCTGOwtodK6ke7TDNap1UNUKGTtlZM2NGEQz.7x1/fZe', // 'admin1234'
    createdAt: faker.date.past(),
    updatedAt: faker.date.past()
  },
  {
    _id: new ObjectId('1bc4c5635e12a4e9de5e112b'),
    name: 'User',
    email: 'user@example.com',
    password: '$2a$05$5FYwNzacRknNOgdYzMMUzO13HZw2/l0bA8qjzSaqA3Ibg6QP71R/K', // 'user1234'
    createdAt: faker.date.past(),
    updatedAt: faker.date.past()
  }
]

