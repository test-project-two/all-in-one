const faker = require('faker')
const { ObjectId } = require('mongoose').Types

const users = ['5aa1c2c35ef7a4e97b5e995a', '1bc4c5635e12a4e9de5e112b']
let tasks = []
while (tasks.length < 10) {
  tasks.push(
    {
      name: faker.commerce.productName(),
      description: faker.lorem.sentences(),
      user: new ObjectId(faker.random.arrayElement(users)),
      createdAt: faker.date.past(),
      updatedAt: faker.date.past()
    }
  )
}

module.exports = tasks
