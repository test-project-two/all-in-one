const crypto = require('crypto')
const IV = crypto.randomBytes(16)
const ALGORITHM = 'aes-256-cbc'
const KEY = crypto.scryptSync(process.env.JWT_SECRET, 'salt', 32)

module.exports = {
  /**
   * Checks is password matches
   * @param {string} password - password
   * @param {Object} user - user object
   * @return {boolean}
   */
  async checkPassword (password, user) {
    return new Promise((resolve, reject) => {
      user.comparePassword(password, (err, isMatch) => {
        if (err) {
          reject(this.buildErrObject(422, err.message))
        }
        resolve(isMatch ? true : false)
      })
    })
  },

  /**
   * Encrypts text
   * @param {string} text - text to encrypt
   * @return {string} crypted - encrypted text
   */
  encrypt (text) {
    const cipher = crypto.createCipheriv(ALGORITHM, KEY, IV)
    let crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex')
    return crypted
  },

  /**
   * Decrypts text
   * @param {string} text - text to decrypt
   * @return {string} decrypted - decrypted text
   */
  decrypt (text) {
    const decipher = crypto.createDecipheriv(ALGORITHM, KEY, IV)
    try {
      let decrypted = decipher.update(text, 'hex', 'utf8')
      decrypted += decipher.final('utf8')
      return decrypted
    } catch (err) {
      return err
    }
  }
}
