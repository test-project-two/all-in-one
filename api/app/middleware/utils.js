const { validationResult } = require('express-validator')

/**
 * Removes extension from file
 * @param {string} file - filename
 * @return {string} - filename without extension
 */
exports.removeExtensionFromFile = file => {
  return file.split('.').slice(0, -1).join('.').toString()
}

/**
 * Handles error by printing to console and builds an error response
 * @param {Object} res - response object
 * @param {Object} err - error object
 */
exports.handleError = (res, err) => {
  // Prints error in console.
  if (process.env.NODE_ENV === 'development') {
    console.log(err)
  }
  // Sends error to user.
  res.status(err.code).json({
    errors: {
      msg: err.message
    }
  })
}

/**
 * Builds error object
 * @param {number} code - error code
 * @param {string} message - error text
 * @return {Object} - object with error code and message
 */
exports.buildErrObject = (code, message) => {
  return {
    code,
    message
  }
}

/**
 * Builds success object
 * @param {string} message - success text
 * @return {Object} - object with success message
 */
exports.buildSuccObject = message => {
  return {
    msg: message
  }
}

/**
 * Item not found
 * @param {Object} err - error object
 * @param {Object} item - item result object
 * @param {Function} reject - reject callback
 * @param {string} message - message
 * @return {Boolean} - result of check the error and the item
 */
exports.itemNotFound = (err, item, reject, message) => {
  switch (true) {
    case !!err:
      reject(this.buildErrObject(422, err.message))
      break
    case !item:
      reject(this.buildErrObject(404, message))
      break
    default:
      // All ok.
      return false
  }
  // We have some issues.
  return true
}

/**
 * Builds error for validation files
 * @param {Object} req - request object
 * @param {Object} res - response object
 * @param {Function} next - next callback
 * @return {*} - execute next callback or error handler
 */
exports.validationResult = (req, res, next) => {
  try {
    validationResult(req).throw()
    return next()
  } catch (err) {
    return this.handleError(res, this.buildErrObject(422, err.array()))
  }
}
