const { buildSuccObject, buildErrObject, itemNotFound } = require('../middleware/utils')

module.exports = {
  
  /**
   * Gets items from database
   * @param {Object} query - query object
   */
  async getItemList (model, query) {
    return new Promise((resolve, reject) => {
      model.find(query, (err, items) => {
        if (err) {
          reject(buildErrObject(422, err.message))
        }
        resolve(items)
      })
    })
  },
  
  
  /**
   * Creates a new item in database
   * @param {Object} req - request object
   */
  async createItem (req, model) {
    return new Promise((resolve, reject) => {
      model.create(req, (err, item) => {
        if (err) {
          reject(buildErrObject(422, err.message))
        }
        resolve(item)
      })
    })
  },
  
  /**
   * Gets item from database by id
   * @param {string} id - item id
   */
  async getItem (id, model) {
    return new Promise((resolve, reject) => {
      model.findById(id, (err, item) => {
        if (!itemNotFound(err, item, reject, 'NOT_FOUND')) resolve(item)
      })
    })
  },
  
  /**
   * Updates an item in database by id
   * @param {string} id - item id
   * @param {Object} req - request object
   */
  async updateItem (id, model, req) {
    return new Promise((resolve, reject) => {
      model.findByIdAndUpdate(id, req, { new: true, runValidators: true },
        (err, item) => {
          if (!itemNotFound(err, item, reject, 'NOT_FOUND')) resolve(item)
        }
      )
    })
  },
  
  /**
   * Deletes an item from database by id
   * @param {string} id - id of item
   */
  async deleteItem (id, model) {
    return new Promise((resolve, reject) => {
      model.findByIdAndRemove(id, (err, item) => {
        if (!itemNotFound(err, item, reject, 'NOT_FOUND')) resolve(buildSuccObject('DELETED'))
      })
    })
  }
  
}
