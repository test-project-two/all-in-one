/*
 * The loading of all files from folder dynamically
 */
const fs = require('fs')
const { removeExtensionFromFile } = require('../middleware/utils')

module.exports = () => {
  fs.readdirSync(__dirname + '/').map(file => {
    const EXCLUDE = ['index.js']
    if (file.match(/\.js$/) !== null && EXCLUDE.indexOf(file) === -1) {
      return require(`./${removeExtensionFromFile(file)}`)
    }
  })
}
