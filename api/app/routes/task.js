const express = require('express')
const router = express.Router()
const authController = require('../controllers/auth')
const controller = require('../controllers/task')
const validate = require('../controllers/task.validate')
const trimRequest = require('trim-request')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', { session: false })

/*
 * Task routes
 */

// Get list of items.
router.get(
  '/list',
  requireAuth,
  controller.getItemList
)

// Create new item.
router.post(
  '/',
  requireAuth,
  trimRequest.all,
  validate.createItem,
  controller.createItem
)

// Get item.
router.get(
  '/:id',
  requireAuth,
  trimRequest.all,
  validate.getItem,
  authController.checkUserAccessPerRoute('task'),
  controller.getItem
)

// Update item.
router.patch(
  '/:id',
  requireAuth,
  trimRequest.all,
  validate.updateItem,
  authController.checkUserAccessPerRoute('task'),
  controller.updateItem
)

// Delete item.
router.delete(
  '/:id',
  requireAuth,
  trimRequest.all,
  validate.deleteItem,
  authController.checkUserAccessPerRoute('task'),
  controller.deleteItem
)

module.exports = router
