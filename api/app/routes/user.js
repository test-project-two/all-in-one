const express = require('express')
const router = express.Router()
const controller = require('../controllers/user')
const trimRequest = require('trim-request')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', { session: false })

/*
 * User routes
 */

// Get user profile.
router.get(
  '/profile',
  requireAuth,
  trimRequest.all,
  controller.getProfile
)

module.exports = router
