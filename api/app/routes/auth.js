const express = require('express')
const router = express.Router()
const controller = require('../controllers/auth')
const validate = require('../controllers/auth.validate')
const trimRequest = require('trim-request')

/*
 * Authentication routes
 */

// todo: need to implement "Refresh token" route.

// Login.
router.post('/login', trimRequest.all, validate.login, controller.login)

module.exports = router
