const express = require('express')
const router = express.Router()
const fs = require('fs')
const { removeExtensionFromFile } = require('../middleware/utils')

/*
 * Load routes.
 */

// Auth route.
router.use('/', require('./auth'))

// All another routes.
fs.readdirSync(__dirname + '/').map(file => {
  const EXCLUDE = ['index.js', 'auth.js']
  if (file.match(/\.js$/) !== null && EXCLUDE.indexOf(file) === -1) {
    file = removeExtensionFromFile(file)
    return router.use(`/${file}`, require(`./${file}`))
  }
})

// Setup route for home page.
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' })
})

// Handle 404 error.
router.use('*', (req, res) => {
  res.status(404).json({
    errors: {
      msg: 'URL_NOT_FOUND'
    }
  })
})

module.exports = router
