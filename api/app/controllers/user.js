const model = require('../models/user')
const { matchedData } = require('express-validator')
const { handleError } = require('../middleware/utils')
const db = require('../middleware/db')

/********************
 * Public functions *
 ********************/

/**
 * Get user profile function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
exports.getProfile = async (req, res) => {
  try {
    res.status(200).json(await db.getItem(req.user._id.toString(), model))
  } catch (error) {
    handleError(res, error)
  }
}
