const { validationResult } = require('../middleware/utils')
const { check } = require('express-validator')

/**
 * Validate request.
 */

// Create new item.
exports.createItem = [
  check('name').exists().withMessage('MISSING').not().isEmpty().withMessage('IS_EMPTY'),
  check('description').optional(),
  (req, res, next) => {
    validationResult(req, res, next)
  }
]

// Create update item.
exports.updateItem = [
  check('id').exists().withMessage('MISSING').not().isEmpty().withMessage('IS_EMPTY').isMongoId().withMessage('ID_MALFORMED'),
  check('name').exists().withMessage('MISSING').not().isEmpty().withMessage('IS_EMPTY'),
  check('description').optional(),
  (req, res, next) => {
    validationResult(req, res, next)
  }
]

// Get item.
exports.getItem = [
  check('id').exists().withMessage('MISSING').not().isEmpty().withMessage('IS_EMPTY').isMongoId().withMessage('ID_MALFORMED'),
  (req, res, next) => {
    validationResult(req, res, next)
  }
]

// Delete item.
exports.deleteItem = [
  check('id').exists().withMessage('MISSING').not().isEmpty().withMessage('IS_EMPTY').isMongoId().withMessage('ID_MALFORMED'),
  (req, res, next) => {
    validationResult(req, res, next)
  }
]
