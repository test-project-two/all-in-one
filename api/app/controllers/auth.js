const jwt = require('jsonwebtoken')
const User = require('../models/user')
const Task = require('../models/task')
const { handleError, buildErrObject, itemNotFound } = require('../middleware/utils')
const { matchedData } = require('express-validator')
const auth = require('../middleware/auth')

/*********************
 * Private functions *
 *********************/

/**
 * Generates a token
 * @param {Object} user - user object
 * @return {string} - signed and encrypted token
 */
const generateToken = user => {
  // Gets expiration time.
  const EXPIRATION = Math.floor(Date.now() / 1000) + 60 *
    (process.env.JWT_EXPIRATION_IN_MINUTES || (24 * 60))
  return auth.encrypt(
    jwt.sign(
      { data: { _id: user }, exp: EXPIRATION },
      process.env.JWT_SECRET
    )
  )
}

/**
 * Creates an object with user info
 * @param {Object} req - request object
 * @return {Object} user - object with user data
 */
const setUserInfo = req => {
  let user = {
    _id: req._id,
    name: req.name,
    email: req.email
  }
  return user
}

/**
 * Returns token
 * @param {Object} user - user object
 * @return {Object} - object with the generated token and user data
 */
const returnToken = user => {
  // Returns data with access token.
  return {
    token: generateToken(user._id),
    user: setUserInfo(user)
  }
}

/**
 * Finds user by email
 * @param {string} email - user´s email
 * @return {*} - result of query (reject() or resolve())
 */
const findUser = async email => {
  return new Promise((resolve, reject) => {
    User.findOne({ email }, 'password name email',
      (err, item) => {
        if (!itemNotFound(err, item, reject, 'USER_DOES_NOT_EXIST')) resolve(item)
      }
    )
  })
}

/**
 * Checks the access of user to route.
 * User should have access only to self user and tasks.
 * @param {Object} data - data object
 * @param {Function} - next callback
 * @return {*} - result of check (execute next callback or error)
 */
const checkPermissions = async (data, next) => {
  return new Promise((resolve, reject) => {
    switch (data.route) {
      case 'user':
        return data.itemId === data.userId.toString() ? resolve(next()) : reject(buildErrObject(401, 'UNAUTHORIZED'))
      case 'task':
        Task.findById(data.itemId, (err, result) => {
          if (!itemNotFound(err, result, reject, 'TASK_NOT_FOUND')) {
            return data.userId.equals(result.user) ? resolve(next()) : reject(buildErrObject(401, 'UNAUTHORIZED'))
          }
        })
    }
  })
}

/********************
 * Public functions *
 ********************/

/**
 * Login function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 * @return {*} - result of login (response token or error)
 */
exports.login = async (req, res) => {
  try {
    const data = matchedData(req)
    const user = await findUser(data.email)
    const isPasswordMatch = await auth.checkPassword(data.password, user)
    if (!isPasswordMatch) {
      handleError(res, buildErrObject(409, 'WRONG_PASSWORD'))
    } else {
      res.status(200).json(returnToken(user))
    }
  } catch (error) {
    handleError(res, error)
  }
}

/**
 * Check user access by user id. User can have access only to self data.
 * @param {Array} roles - roles specified on the route
 * @return {*} - result of check (execute next callback or response error)
 */
exports.checkUserAccessPerRoute = route => async (req, res, next) => {
  try {
    const params = matchedData(req)
    const data = {
      itemId: params.id,
      userId: req.user._id,
      route
    }
    await checkPermissions(data, next)
  } catch (error) {
    handleError(res, error)
  }
}
