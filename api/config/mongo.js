const mongoose = require('mongoose')
const MONGO_URI = process.env.MONGO_URI
const loadModels = require('../app/models')

module.exports = () => {
  const connect = () => {
    mongoose.connect(
      MONGO_URI,
      {
        keepAlive: true,
        reconnectTries: Number.MAX_VALUE,
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify: false
      },
      err => {
        console.log(err ? `Error connecting to DB: ${err}` : 'DB Connection: OK')
      }
    )
  }
  connect()

  mongoose.connection.on('error', console.log)
  mongoose.connection.on('disconnected', connect)

  loadModels()
}
