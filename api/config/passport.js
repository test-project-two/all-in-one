const passport = require('passport')
const User = require('../app/models/user')
const auth = require('../app/middleware/auth')
const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt

/**
 * Decrypt token after the extract
 * @param {Object} req - request object
 * @returns {string} token - decrypted token
 */
const ExtractJwtDecrypt = req => {
  let token = ExtractJwt.fromAuthHeaderAsBearerToken()(req)
  if (token) {
    // Decrypts token.
    token = auth.decrypt(token)
  }
  return token
}

/**
 * Options object for jwt middlware.
 */
const jwtOptions = {
  jwtFromRequest: ExtractJwtDecrypt,
  secretOrKey: process.env.JWT_SECRET
}

/**
 * Login with JWT middleware
 */
const jwtLogin = new JwtStrategy(jwtOptions, (payload, done) => {
  User.findById(payload.data._id, (err, user) => {
    if (err) {
      return done(err, false)
    }
    return !user ? done(null, false) : done(null, user)
  })
})

passport.use(jwtLogin)
