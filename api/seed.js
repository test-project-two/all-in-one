const { Seeder } = require('mongo-seeding')
const path = require('path')
const config = {
  database: process.env.MONGO_URI,
  dropDatabase: true
}
const seeder = new Seeder(config)
const collections = seeder.readCollectionsFromPath(path.resolve('./data'))

const main = async () => {
  await seeder.import(collections).catch(err => console.log(err))
  console.log('Seed complete!')
}

main()
