const createError = require('http-errors')
const compression = require('compression')
const cookieParser = require('cookie-parser')
const cors = require('cors')
const express = require('express')
const logger = require('morgan')
const path = require('path')
const passport = require('passport')
const initMongo = require('./config/mongo')
// Setup passport.
require('./config/passport')

const app = express()

// View engine setup.
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'jade')

// Enable only in development HTTP request logger middleware.
if (process.env.NODE_ENV === 'development') {
  app.use(logger('dev'))
}

// Init all other stuffs.
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())
app.use(passport.initialize())
app.use(compression())
app.use(require('./app/routes'))

// Init MongoDB.
initMongo()

// Catch 404 and forward to error handler.
app.use(function (req, res, next) {
  next(createError(404))
})

// Error handler.
app.use(function (err, req, res, next) {
  // Set locals, only providing error in development.
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // Render the error page.
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
