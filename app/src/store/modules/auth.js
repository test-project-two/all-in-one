import { setAuthToken } from '../../utils/auth'
import { checkErrorData } from '../../utils/assist'
import axios from '../../services/axiosInstance'

const http = axios.instance

const state = {
  token: {},
  user: {
    authenticated: false
  }
}

const getters = {
  isLoggedIn (state) {
    return state.user.authenticated ? true : false
  },
  currentUser (state) {
    return state.user
  }
}

const mutations = {
  setToken (state, token) {
    setAuthToken(token, axios)
    state.token = token
  },
  setUser (state, user) {
    state.user = user
  }
}

const actions = {
  async login ({ commit }, model) {
    try {
      const response = await http.post('/login', model)
      commit('setToken', response.data.token)
      commit('setUser', Object.assign({ authenticated: true }, response.data.user))
      return response.data
    } catch (err) {
      let errors = checkErrorData(err)
      // eslint-disable-next-line no-console
      console.log('Login error:', errors ? errors : err)
      throw errors ? errors : err
    }
  },
  logout ({ commit }) {
    commit('setToken', null)
    commit('setUser', { authenticated: false })
    // eslint-disable-next-line no-console
    console.log('User is logged out!')
  },
  async getProfile ({ commit }) {
    try {
      const response = await http.get('/user/profile')
      commit('setUser', Object.assign({ authenticated: true }, response.data))
      return response.data
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log('User is not logged! Let\'s login!')
      throw err
    }
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
