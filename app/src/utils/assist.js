/**
 * Get list of errors
 * @return {*} - response.data.errors (if exist) or false.
 */
export function checkErrorData (err) {
  return err.response && err.response.data ? err.response.data.errors : false
}

/**
 * Get type of object
 * @return {string} - type of object.
 */
export function typeOf (obj) {
  const toString = Object.prototype.toString
  const map = {
    '[object Boolean]': 'boolean',
    '[object Number]': 'number',
    '[object String]': 'string',
    '[object Function]': 'function',
    '[object Array]': 'array',
    '[object Date]': 'date',
    '[object RegExp]': 'regExp',
    '[object Undefined]': 'undefined',
    '[object Null]': 'null',
    '[object Object]': 'object'
  }
  return map[toString.call(obj)]
}

/**
 * Deep copy of object
 * @return {Object} - copy of object.
 */
export function deepCopy (data) {
  const t = typeOf(data)
  let o
  
  if (t === 'array') {
    o = []
  } else if (t === 'object') {
    o = {}
  } else {
    return data
  }
  
  if (t === 'array') {
    for (let i = 0; i < data.length; i++) {
      o.push(deepCopy(data[i]))
    }
  } else if (t === 'object') {
    for (let i in data) {
      o[i] = deepCopy(data[i])
    }
  }
  return o
}

/**
 * Calculate the hash code value from string value
 * @return {number} - hash code (integer).
 */
export function hashCode (str) {
  let hash = 0
  for (let i = 0, l = str.length; i < l; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash)
  }
  return hash
}
