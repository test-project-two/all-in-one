/**
 * Get auth Bearer token
 * @return {string} - auth Bearer token form Local storage.
 */
export function getAuthToken () {
  return localStorage.getItem('bearerToken')
}

/**
 * Set auth Bearer token
 * @param {string} token - auth Bearer token
 * @param {Object} instance - Axios instance
 */
export function setAuthToken (token, axios) {
  localStorage.setItem('bearerToken', token)
  if (axios) {
    axios.updateAuthorizationHeader(token)
  }
}
