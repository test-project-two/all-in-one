import axios from 'axios'

/**
 * A wrapper around an axios instance,
 * preconfigured to have a base URL and auth headers.
 */
class Axios {
  constructor (baseUrl, token) {
    this.instance = axios.create({
      baseURL: baseUrl,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  }
  
  /**
   * Update Authorization in the Axios instance.
   * @param {string} token - auth Bearer token
   */
  updateAuthorizationHeader (token) {
    this.instance.defaults.headers.Authorization = `Bearer ${token}`
  }
}

export default Axios
