import { getAuthToken } from '../utils/auth'
import Axios from './Axios'



/**
 * Setup Axios instance.
 */
export default new Axios(
  process.env.VUE_APP_API_URL || 'http://0.0.0.0:3000', getAuthToken()
)
