import Vue from 'vue'
import Router from 'vue-router'
import Store from '@/store'
import Login from './views/Login.vue'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/profile',
      name: 'profile',
      // Route level code-splitting.
      // This generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/Profile.vue')
    }
  ]
})

// Navigation guards.
router.beforeEach((to, from, next) => {
  const isLoggedIn = Store.getters['auth/isLoggedIn']
  switch (to.name) {
    case 'login':
      next(!isLoggedIn ? true : { name: 'profile' })
      break
    default:
      next(isLoggedIn ? true : { name: 'login' })
  }
})

export default router
