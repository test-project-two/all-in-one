export default {
  methods: {
    onSave () {
      // Allow custom functionality for alter data before submit.
      if (typeof this.beforeSave === 'function') this.beforeSave()
      else if (this.$refs.form.validate()) this.$emit('on-save', typeof this.cleanDataBeforeSave === 'function' ? this.cleanDataBeforeSave(this.model) : this.model)
    },
    onCancel () {
      // Allow custom functionality before cancel form.
      if (typeof this.beforeCancel === 'function') this.beforeCancel()
      else {
        this.form.loading = false
        this.form.show = false
      }
    }
  }
}
