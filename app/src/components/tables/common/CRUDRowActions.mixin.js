export default {
  methods: {
    onUpdateItem (item) {
      this.$emit('on-update-item', { item })
    },
    onDeleteItem (item) {
      this.$emit('on-delete-item', { item })
    }
  }
}
