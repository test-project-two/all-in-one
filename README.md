# Test project
> Vue.js (front) with Express.js (backend) project

## Getting started
Full setup development environment of project:

```bash
$ make
```


### Prerequisites

#### Start project
Full setup development environment of project.
Stops and starts docker containers, starts API and APP.
But, without update of containers images.
Create demo content.


```bash
$ make
```

#### Stop project

```bash
$ make down
```

#### Rebuild docker images and start project
Setup development enviroronment of project.
Stops and starts docker containers, with update of images.

```bash
$ make rebuild
```

#### Create content
Drop database and generate the new demo content.

```bash
$ make seed
```
